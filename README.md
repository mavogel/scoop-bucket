# scoop-bucket
- for testing https://gitlab.com/mavogel/release-testing
- see commands: https://github.com/lukesampson/scoop/wiki/Commands

## Prerequisites
From https://scoop.sh/
- https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/how-to-determine-which-versions-are-installed
- https://stackoverflow.com/questions/1825585/determine-installed-powershell-version

## Installing
```sh
#scoop bucket add <name-of-bucket> <location-of-git-repo>
scoop bucket add mavogel-tools https://gitlab.com/mavogel/scoop-bucket.git
scoop bucket list # -> you should see 'mavogel-tools'
scoop search release-testing # -> you should see release-testing listed under, 'mavogel-tools bucket:'
scoop install release-testing
release-testing -r
```

## Updating
```sh
scoop update
```